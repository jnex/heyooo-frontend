export const environment = {
    production: true,
    firebase: {
        apiKey: 'AIzaSyCDQYiUcT93l590rFSfWhKDilmmymucdvk',
        authDomain: 'heyooo-mail.firebaseapp.com',
        databaseURL: 'https://heyooo-mail.firebaseio.com',
        projectId: 'heyooo-mail',
        storageBucket: 'heyooo-mail.appspot.com',
        messagingSenderId: '1064571120460'
    },
    api: {
        host: 'heyooo.jnexsoft.com',
        port: 443,
        ssl: true
    }
};
