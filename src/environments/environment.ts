// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyCDQYiUcT93l590rFSfWhKDilmmymucdvk',
        authDomain: 'heyooo-mail.firebaseapp.com',
        databaseURL: 'https://heyooo-mail.firebaseio.com',
        projectId: 'heyooo-mail',
        storageBucket: 'heyooo-mail.appspot.com',
        messagingSenderId: '1064571120460'
    },
    api: {
        host: 'localhost',
        port: 3333,
        ssl: false
    }
};
