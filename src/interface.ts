import { Response as Res } from './app/services/api/contract';

export interface Email {
    id: number;
    // heading: string;
    // email: string;
    subject: string;
    body: string;
    to: { address: string, name: string }[];
    from: { address: string, name: string };
    cc?: { address: string, name: string }[];
    bcc?: { address: string, name: string }[];
    date: string;
    flags: string[];
}

export interface EmailResponse {
    'headers': EmailHeaders;
    'attrs': { struck: Object[], date: string, flags: string[], uid: number, modseq: number };
    'body': string;
}

export interface EmailHeaders {
    'to': EmailTo;
    'cc': EmailTo;
    'bcc': EmailTo;
    'from': EmailTo;
    'subject': string;
}

export interface EmailTo {
    value: { address: string, name: string }[];
    html: string;
    text: string;
}

export interface SampleData {
    inbox: Email[];
    starred: Email[];
    sent: Email[];
    draft: Email[];
    spam: Email[];
    trash: Email[];
    archive: Email[];
}

export interface Response extends Res {
    extend?: null;
}
