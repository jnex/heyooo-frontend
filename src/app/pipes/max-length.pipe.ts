import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'maxLength'
})
export class MaxLengthPipe implements PipeTransform {

  transform(value: string, args: number = 15): any {
      let transitName;
      let finalName;
      let i = 1;
      const iniName = value.split(' ');
      finalName = iniName[0];

      while (i < iniName.length) {
          transitName = finalName + ' ' + iniName[i];
          if (transitName.length <= args) {
              finalName = transitName;
          }
          i++;
      }
    return finalName;
  }

}
