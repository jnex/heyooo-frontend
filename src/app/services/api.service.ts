import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ContractService as ApiContract } from './api/contract.service';
import Storage from './storage.service';
import { Response } from './api/contract';
import { Observable } from 'rxjs/observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ApiService extends ApiContract {

    private _currentMailAccount: BehaviorSubject<number>;
    public currentMailAccount: Observable<number>;

    constructor(private sHttp: HttpClient) {
        super(sHttp);
        this._currentMailAccount = new BehaviorSubject<number>(parseInt(Storage.get('selected_email'), 10));
        this.currentMailAccount = this._currentMailAccount.asObservable();
    }

    fetchEmails(): Observable<Object> {
        return this.get('email/list');
    }

    fetchMailBox(box: string, id: number) {
        // const id = Storage.get('selected_email');
        return this.post('email/fetch', { email_id: id, box: box });
    }

    nextMailAccount() {
        const id = parseInt(Storage.get('selected_email'), 10);
        if (id > 0) {
            this._currentMailAccount.next(id);
        }
    }

    sendEmail(data): Observable<Object> {
        return this.post('email/send', data);
    }

}
