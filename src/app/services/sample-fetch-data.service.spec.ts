import { TestBed, inject } from '@angular/core/testing';

import { SampleFetchDataService } from './sample-fetch-data.service';

describe('SampleFetchDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SampleFetchDataService]
    });
  });

  it('should be created', inject([SampleFetchDataService], (service: SampleFetchDataService) => {
    expect(service).toBeTruthy();
  }));
});
