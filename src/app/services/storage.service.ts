import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {

    static set(item: string, data: string): string {
        if (!localStorage.getItem(item)) {
            localStorage.removeItem(item);
        }
        localStorage.setItem(item, data);
        return item;
    }

    static get(item: string, $default: string = ''): string {
        return (localStorage.getItem(item) || $default);
    }

    static clear(): void {
        return localStorage.clear();
    }

    static delete(item): void {
        return localStorage.removeItem(item);
    }

    static key(item: number): string {
        return localStorage.key(item);
    }

    static get len(): number {
        return localStorage.length;
    }
}

export default StorageService;
