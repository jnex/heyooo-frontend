import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SampleFetchDataService } from './sample-fetch-data.service';
import { ApiService } from './api.service';
import { Response } from './api/contract';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Email, EmailResponse } from '../../interface';
import { Observable } from 'rxjs/observable';
import Storage from './storage.service';

@Injectable()
export class FetchDataService extends SampleFetchDataService {

    public currentInboxList: Email[];
    private _currentMailBox: BehaviorSubject<string>;
    public currentMailbox: Observable<string>;
    private _readCurrentEmail: BehaviorSubject<number>;
    public readCurrentEmail: Observable<number>;
    private _emailAccountList: BehaviorSubject<Object>;
    public emailAccountList: Observable<Object>;

    constructor(public api: ApiService, private http: HttpClient) {
        super(http);
        this._currentMailBox = new BehaviorSubject<string>('inbox');
        this.currentMailbox = this._currentMailBox.asObservable();
        this._readCurrentEmail = new BehaviorSubject<number>(0);
        this.readCurrentEmail = this._readCurrentEmail.asObservable();
        const emails = JSON.parse(Storage.get('emailList', '[]'));
        this._emailAccountList = new BehaviorSubject<Object>(emails);
        this.emailAccountList = this._emailAccountList.asObservable();
    }


    selectedMailBox(id: number) {
        return new Promise((resolve, reject) => {
            this.currentMailbox.subscribe((mailbox: string) => {
                this.api.fetchMailBox(mailbox, id).subscribe((response: Response) => {
                    const emails = <EmailResponse[]> response.data.mails;
                    const temp = emails.map(email => {
                        return {
                            id : email.attrs.uid,
                            to: email.headers.to.value,
                            from: email.headers.from.value[0],
                            body: email.body,
                            subject: email.headers.subject,
                            date: email.attrs.date,
                            flags: email.attrs.flags
                        };
                    });
                    resolve(temp);
                });
        });
            // const data = <Email[]> this.fetchData.fetch(mailbox);
            // if (data) {
            //     this.inboxList = data;
            // }
        });
    }

    changeCurrentEmail(id: number) {
        this._readCurrentEmail.next(id);
    }

    changeMailBox(mailBox: string) {
        this._currentMailBox.next(mailBox);
    }

    newEmailAccList() {
        this._emailAccountList.next(JSON.parse(Storage.get('emailList')));
    }

}
