import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Email, SampleData } from '../../Interface';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/observable';

@Injectable()
export class SampleFetchDataService {

    private _data: SampleData;

    constructor(private sHttp: HttpClient) {}

    fetch($type: string = 'all'): Email[] | SampleData {
        if (!this._data) {
            return null;
        }

        if ($type === 'all') {
            return this._data;
        }

        if (this._data[$type]) {
            return this._data[$type];
        }
        return null;
    }

    inbox(): Email[] {
        return this._data.inbox;
    }

    starred(): Email[] {
        return this._data.starred;
    }

    sent(): Email[] {
        return this._data.sent;
    }

    draft(): Email[] {
        return this._data.draft;
    }

    spam(): Email[] {
        return this._data.spam;
    }

    trash(): Email[] {
        return this._data.trash;
    }

    archive(): Email[] {
        return this._data.archive;
    }

    get data(): object {
        return this._data;
    }

}
