import { Email, EmailResponse } from '../../../interface';
import { environment } from '../../../environments/environment.prod';

export namespace ContractInterface {
    export const token = 'auth_token';
    export const ssl = environment.api.ssl;
    export const host = environment.api.host;
    export const port = environment.api.port;
    export const prefix = 'api';
    export const version = 'v1';
    export const protocol = ssl ? 'https://' : 'http://';

    export function buildUrl(location: string): string {
        if (location[0] === '/') {
            location = location.slice(1);
        }

        return [buildHost(), prefix, version, location].join('/');
    }

    export function buildHost(): string {
        return `${protocol}${host}:${port}`;
    }
}

export interface Response {
    status: 'ok' | 'error';
    code: number;
    data: Data;
    error?: string;
}

export interface Data {
    email?: EmailAccount;
    emails?: EmailAccount[];
    mail?: EmailResponse;
    mails: EmailResponse[];
    msg?: string;
    login?: { type: string, token: string, refreshToken: string, user: {} };
}

export interface EmailAccount {
    id: number;
    name: string;
    email: string;
    primary: boolean;
}

export interface ComposeEmail {
    to:  string[] | NameEmail[];
    email_id: number;
    body: string;
    subject: string;
    cc:  string[] | NameEmail[];
    bcc:  string[] | NameEmail[];
}

export interface NameEmail {
    name: string;
    email: string;
}

export interface ApiDefaults {
    ssl: boolean;
    host: string;
    port: number;
    prefix: string;
}
