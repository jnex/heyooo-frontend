import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';

import { ContractInterface as Contract, ApiDefaults } from './contract';
import Storage from '../storage.service';


@Injectable()
export class ContractService {

    private headers: HttpHeaders;
    private key: string;
    private options: { headers: HttpHeaders };

    constructor(private http: HttpClient) { }

    private getAuthToken($default: string = '') {
        this.key = 'Bearer ' + Storage.get(Contract.token, $default);
    }

    get(url: string, options: { headers: Array<any> } | boolean = null): Observable<object> {
        url = Contract.buildUrl(url);
        const addHeaders = typeof options !== 'boolean';
        if (typeof options !== 'boolean') {
            this.setHeaders(options);
            return this.http.get(url, this.options);
        }
        return this.http.get(url);
    }

    delete(url: string, options: { headers: Array<any> } | boolean = null): Observable<object> {
        url = Contract.buildUrl(url);
        const addHeaders = typeof options !== 'boolean';
        if (typeof options !== 'boolean') {
            this.setHeaders(options);
            return this.http.delete(url, this.options);
        }
        return this.http.delete(url);
    }

    put(url: string, data = {}, options: { headers: Array<any> } | boolean = null): Observable<object> {
        url = Contract.buildUrl(url);
        const addHeaders = typeof options !== 'boolean';
        if (typeof options !== 'boolean') {
            this.setHeaders(options);
            return this.http.put(url, data = {}, this.options);
        }
        return this.http.put(url, data = {});
    }

    patch(url: string, data: object = {}, options: { headers: Array<any> } | boolean = null): Observable<object> {
        url = Contract.buildUrl(url);
        const addHeaders = typeof options !== 'boolean';
        if (typeof options !== 'boolean') {
            this.setHeaders(options);
            return this.http.patch(url, data, this.options);
        }
        return this.http.patch(url, data);
    }

    post(url: string, data: object = {}, options: { headers: Array<any> } | boolean = null): Observable<object> {
        url = Contract.buildUrl(url);
        const addHeaders = typeof options !== 'boolean';
        if (typeof options !== 'boolean') {
            this.setHeaders(options);
            return this.http.post(url, data, this.options);
        }
        return this.http.post(url, data);
    }

    setHeaders(options: { headers: Array<any> }): ContractService {

        this.getAuthToken();
        this.headers = new HttpHeaders({
            Authorization: this.key
        });
        this.options = { headers: this.headers };

        if (options && options.headers) {
            const headers = options.headers;
            for (const header in headers) {
                if (headers.hasOwnProperty(header)) {
                    this.headers.append(header, headers[header]);
                }
            }
        }
        return this;
    }

}
