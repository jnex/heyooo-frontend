import { Injectable } from '@angular/core';
import { ContractInterface as Contract, Response } from './api/contract';
import { ApiService } from './api.service';
import Storage from './storage.service';

@Injectable()
export class AuthService {

    private token = '';

    constructor(private api: ApiService) { }

    login (username, password): Promise<string> {
        return new Promise((resolve, reject) => {
            this.api.post('login', { username: username, password: password }, false).subscribe((res: Response) => {
                console.log(res);
                if (res.status === 'ok') {
                    const login = res.data.login;
                    Storage.set('auth_token', login.token);
                    Storage.set('ref_token', login.refreshToken);
                    Storage.set('user', JSON.stringify(login.user));
                    resolve(login.token);
                }
                reject(res);
            }, err => reject(err));
        });
    }

    async logout() {
        Storage.clear();
        return true;
    }

    authenticated(): boolean {
        return !!Storage.get(Contract.token, null);
    }

}
