import { NgModule } from '@angular/core';

import { FirebaseAuthService } from '../services/firebase.auth.service';
import { ContractService } from '../services/api/contract.service';
import { ApiService } from '../services/api.service';
import { FetchDataService } from '../services/fetch-data.service';
import { SampleFetchDataService } from '../services/sample-fetch-data.service';

import { AuthenticationGuard } from '../guards/authentication.guard';

import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../../environments/environment';

@NgModule({
    imports: [
        AngularFireAuthModule,
        AngularFireModule.initializeApp(environment.firebase),
    ],
    declarations: [],
    providers: [
        FirebaseAuthService,
        AuthenticationGuard,
        FetchDataService,
        SampleFetchDataService,
        ContractService,
        ApiService
    ],

})
export class CoreModule { }
