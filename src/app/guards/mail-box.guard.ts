import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import Storage from '../services/storage.service';

@Injectable()
export class MailBoxGuard implements CanActivate {

    constructor(private router: Router) { }
    canActivate() {
        console.log('aa')
        if (!Storage.get('emailList')) {
            this.router.navigate(['/dashboard/addMailBox']);
        }
        return true;
    }
}
