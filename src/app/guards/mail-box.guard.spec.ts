import { TestBed, async, inject } from '@angular/core/testing';

import { MailBoxGuard } from './mail-box.guard';

describe('MailBoxGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MailBoxGuard]
    });
  });

  it('should ...', inject([MailBoxGuard], (guard: MailBoxGuard) => {
    expect(guard).toBeTruthy();
  }));
});
