import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import Storage from '../services/storage.service';
import { ContractInterface as Contract } from '../services/api/contract';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';


@Injectable()
export class AuthenticationGuard implements CanActivate {

    constructor(private router: Router, private af: AngularFireAuth) {}

    canActivate(): Observable<boolean> | boolean {
        if (!Storage.get(Contract.token)) {
            this.router.navigate(['/signin']);
            return false;
        }

        return true;
    //   return this.af.authState
    //     .take(1)
    //     .map((authState) => !!authState)
    //     .do(authenticated => {
    //       if (!authenticated) {
    //             this.router.navigate(['/signin']);
    //         }
    //     });
    }
}
