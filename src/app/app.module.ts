import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { MailBoxComponent } from './components/mail-box/mail-box.component';
import { ReadEmailComponent } from './components/read-email/read-email.component';
import { TopNavigationComponent } from './components/top-navigation/top-navigation.component';
import { LeftNavigationComponent } from './components/left-navigation/left-navigation.component';
import { ComposeEmailComponent } from './components/compose-email/compose-email.component';
import { AddMailBoxComponent } from './components/add-mail-box/add-mail-box.component';
import { SignupComponent } from './components/signup/signup.component';
import { SigninComponent } from './components/signin/signin.component';
import { RoutesComponent } from './components/routes/routes.component';
import { EmailLayoutComponent } from './components/email-layout/email-layout.component';
import { AddMailServerComponent } from './components/add-mail-server/add-mail-server.component';
import { WysiwygEditorComponent } from './components/wysiwyg-editor/wysiwyg-editor.component';
import { SettingsLayoutComponent } from './components/settings/settings-layout/settings-layout.component';
import { AccountComponent } from './components/settings/account/account.component';
import { MailboxComponent } from './components/settings/mailbox/mailbox.component';
import { MoreSettingsLayoutComponent } from './components/settings/more-settings-layout/more-settings-layout.component';
import { SignatureComponent } from './components/settings/signature/signature.component';
import { MaxLengthPipe } from './pipes/max-length.pipe';
import { KeepHtmlPipe } from './pipes/keep-html.pipe';
import { StringToDatePipe } from './pipes/string-to-date.pipe';
import { AuthModule } from './auth/auth.module';

@NgModule({
    declarations: [
        AppComponent,
        MailBoxComponent,
        ReadEmailComponent,
        TopNavigationComponent,
        LeftNavigationComponent,
        ComposeEmailComponent,
        AddMailBoxComponent,
        SignupComponent,
        SigninComponent,
        RoutesComponent,
        EmailLayoutComponent,
        AddMailServerComponent,
        WysiwygEditorComponent,
        SettingsLayoutComponent,
        AccountComponent,
        MailboxComponent,
        MoreSettingsLayoutComponent,
        SignatureComponent,
        MaxLengthPipe,
        KeepHtmlPipe,
        StringToDatePipe,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        CoreModule,
        AuthModule,
        FormsModule,
        HttpClientModule,
        BrowserAnimationsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
