import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthenticationGuard } from './guards/authentication.guard';

import { SigninComponent } from './components/signin/signin.component';
import { RoutesComponent } from './components/routes/routes.component';
import { SignupComponent } from './components/signup/signup.component';
import { EmailLayoutComponent } from './components/email-layout/email-layout.component';
import { AddMailBoxComponent } from './components/add-mail-box/add-mail-box.component';
import { AddMailServerComponent } from './components/add-mail-server/add-mail-server.component';
import { SettingsLayoutComponent } from './components/settings/settings-layout/settings-layout.component';
import { SignatureComponent } from './components/settings/signature/signature.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/dashboard/mail',
        pathMatch: 'full'
    },
    {
        path: 'dashboard',
        component: RoutesComponent,
        canActivate: [AuthenticationGuard],

        children: [
            {
                path: 'mail',
                component: EmailLayoutComponent,
                pathMatch: 'full'
            },
            {
                path: 'addMailBox',
                component: AddMailBoxComponent,
                pathMatch: 'full'
            },
            {
                path: 'addMailServer',
                component: AddMailServerComponent,
                pathMatch: 'full'
            },
            {
                path: 'settings',
                component: SettingsLayoutComponent,
            }
        ]
    },
    {
        path: 'signin',
        component: SigninComponent,
    },
    {
        path: 'signup',
        component: SignupComponent,
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
