import {Component} from '@angular/core';

@Component({
    selector: 'heyooo-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'Heyooo Mail';
}
