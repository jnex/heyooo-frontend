import { Component, OnInit } from '@angular/core';
import { FirebaseAuthService } from '../../services/firebase.auth.service';

@Component({
    selector: 'heyooo-routes',
    templateUrl: './routes.component.html',
    styleUrls: ['./routes.component.scss']
})
export class RoutesComponent implements OnInit {

    constructor(private auth: FirebaseAuthService) { }

    ngOnInit() {
    }

}
