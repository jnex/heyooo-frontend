
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { trigger, transition, animate, query, style, stagger, keyframes } from '@angular/animations';
import { AuthService } from '../../services/auth.service';
import { ApiService } from '../../services/api.service';
import Storage from '../../services/storage.service';
import { ContractInterface as authContract, Response } from '../../services/api/contract';


@Component({
    selector: 'heyooo-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.scss'],
    animations: [
      trigger('fade', [
        transition('void => *', [
            style({ opacity: 0}),
            animate(300)
        ])
      ])
    ]
})
export class SigninComponent implements OnInit {

    public username: string;
    public password: string;

    public weChatEnable: boolean;
    public error: string;
    public message: { msg: string, class: string};

    constructor(private auth: AuthService, private router: Router, private api: ApiService) { }

    login(): void {
        this.auth.login(this.username, this.password).then(this.loggedIn.bind(this)).catch(this.loginError.bind(this));
    }


    async loggedIn() {
        this.error = null;
        this.message = { class: 'success', msg: 'Login successful. Redirecting...'};
        this.api.fetchEmails().subscribe((res: Response) => {
            const emails = res.data.emails;
            if (emails.length === 0) {
                this.router.navigateByUrl('dashboard/addMailBox');
                return;
            }
            Storage.set('selected_email', emails[0].id.toString());
            Storage.set('emailList', JSON.stringify(res.data.emails));
            this.router.navigateByUrl('/');
        });
    }

    loginError(err): void {
        if (err.status === 401) {
            this.error = 'Username or Password is Wrong';
        } else {
            this.error = err.message;
        }
    }

    ngOnInit() {
        this.weChatEnable = false;
    }

}
