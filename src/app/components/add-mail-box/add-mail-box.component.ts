import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { FirebaseAuthService } from '../../services/firebase.auth.service';
import { ContractService } from '../../services/api/contract.service';
import { Response } from '../../services/api/contract';
import { ApiService } from '../../services/api.service';
import { FetchDataService } from '../../services/fetch-data.service';
import Storage from '../../services/storage.service';
import * as toastr from 'toastr';

@Component({
    selector: 'heyooo-add-mail-box',
    templateUrl: './add-mail-box.component.html',
    styleUrls: ['./add-mail-box.component.scss']
})
export class AddMailBoxComponent implements OnInit {

    email: string;
    password: string;

    constructor(
        public auth: FirebaseAuthService,
        private api: ContractService,
        private router: Router,
        private location: Location,
        private apiServ: ApiService,
        private fetchData: FetchDataService
    ) { }

    cancelClicked() {
        this.location.back();
    }

    addMailBox(email, password) {
        const data = {
            'email': email,
            'password': password,
            'incoming': 'imap',
            'name': 'Sample Name'
        };

        this.api.post('email/add', data).subscribe((res: Response) => {
            toastr.success('Mail Box Added Successfully', {timeOut: 3000});
            this.apiServ.fetchEmails().subscribe((result: Response) => {
                const emails = result.data.emails;
                Storage.set('emailList', JSON.stringify(emails));
                Storage.set('selected_email', emails[emails.length - 1].id.toString());
                this.fetchData.newEmailAccList();
            });
            this.router.navigate(['/']);
        }, err => {
            toastr.error(err.error.error, {timeOut: 3000});
        });
    }

    ngOnInit() {
    }

}
