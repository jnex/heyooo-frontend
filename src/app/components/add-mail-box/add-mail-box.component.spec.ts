import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMailBoxComponent } from './add-mail-box.component';

describe('AddMailBoxComponent', () => {
    let component: AddMailBoxComponent;
    let fixture: ComponentFixture<AddMailBoxComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ AddMailBoxComponent ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AddMailBoxComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
