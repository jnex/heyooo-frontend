import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { trigger, transition, animate, query, style, stagger, keyframes } from '@angular/animations';
import * as toastr from 'toastr';
import { ApiService } from '../../services/api.service';
import { Response } from '../../services/api/contract';

@Component({
    selector: 'heyooo-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [
      trigger('fade', [
        transition('void => *', [
            style({ opacity: 0}),
            animate(300)
        ])
      ])
    ]
})
export class SignupComponent implements OnInit {

    public name: string;
    public username: string;
    public password: string;

    // public Cpassword: string;
    public chkBox: string;
    public message: { msg: string, class: string};
    public confirmPwd = true;

    constructor(private api: ApiService, private router: Router) { }

    signUp(): void {
        this.api.post('register', { name: this.name, password: this.password, username: this.username }).subscribe((res: Response) => {
            if (res.status === 'ok') {
                toastr.success('Signup Successful....Login Now to continue', {timeOut: 3000});
                this.router.navigateByUrl('/signin');
                return;
            }
            this.message = { class: 'danger', msg: res.error};
            const errorCode = res.code;
        });
    }

    ngOnInit() {
    }

}
