import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMailServerComponent } from './add-mail-server.component';

describe('AddMailServerComponent', () => {
  let component: AddMailServerComponent;
  let fixture: ComponentFixture<AddMailServerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMailServerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMailServerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
