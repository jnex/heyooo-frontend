import { Component, OnInit } from '@angular/core';
import { FirebaseAuthService } from '../../../services/firebase.auth.service';

@Component({
  selector: 'heyooo-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  constructor(public auth: FirebaseAuthService) { }

  ngOnInit() {
  }

}
