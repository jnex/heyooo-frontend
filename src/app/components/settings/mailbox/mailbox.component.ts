import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Response } from '../../../services/api/contract';
import { FetchDataService } from '../../../services/fetch-data.service';
import { Email } from '../../../../interface';
import Storage from '../../../services/storage.service';

@Component({
    selector: 'heyooo-mailbox',
    templateUrl: './mailbox.component.html',
    styleUrls: ['./mailbox.component.scss']
})
export class MailboxComponent implements AfterViewInit {

    public emailList: any[] = [];

    constructor(public api: ApiService, private fetchData: FetchDataService) { }

    delete(card) {
        this.api.delete('email?email_id=' + card.id).subscribe((res: Response) => {
            if (res.code == 200) {
                this.api.fetchEmails().subscribe((result: Response) => {
                    Storage.set('emailList', JSON.stringify(result.data.emails));
                    this.fetchData.newEmailAccList();
                });
            }
        });
    }

    ngAfterViewInit() {
        this.fetchData.emailAccountList.subscribe((res: Email[]) => {
            this.emailList = res;
        })
    }

}
