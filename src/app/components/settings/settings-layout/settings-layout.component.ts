import { Component, OnInit } from '@angular/core';
import { FirebaseAuthService } from '../../../services/firebase.auth.service';

@Component({
  selector: 'heyooo-settings-layout',
  templateUrl: './settings-layout.component.html',
  styleUrls: ['./settings-layout.component.scss']
})
export class SettingsLayoutComponent implements OnInit {

  constructor(public auth: FirebaseAuthService) { }

  ngOnInit() {
  }

}
