import { Component, OnInit } from '@angular/core';
import { Email, EmailResponse } from '../../../interface';
import { FetchDataService } from '../../services/fetch-data.service';
import { ApiService } from '../../services/api.service';

@Component({
    selector: 'heyooo-mail-box',
    templateUrl: './mail-box.component.html',
    styleUrls: ['./mail-box.component.scss']
})
export class MailBoxComponent implements OnInit {

    public inboxList;

    constructor(private fetchData: FetchDataService, private api: ApiService) { }

    ngOnInit() {
        this.api.currentMailAccount.subscribe(res => {
            this.inboxList = null;
            this.fetchData.selectedMailBox(res)
                .then(emailList => this.inboxList = emailList);
        });

    }

    toggleStar(i: number): void {
        this.inboxList[i].starred = !this.inboxList[i].starred;
    }

    mailSelected(i: number): void {
        this.inboxList[i].selected = !this.inboxList[i].selected;
    }

    readNextEmail(emailIndex) {
        this.fetchData.changeCurrentEmail(emailIndex);
    }
}
