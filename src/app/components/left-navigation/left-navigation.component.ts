import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Response } from '../../services/api/contract';
import Storage from '../../services/storage.service';
import { FetchDataService } from '../../services/fetch-data.service';
import { Email, EmailResponse } from '../../../interface';


declare var $: any;

@Component({
    selector: 'heyooo-left-navigation',
    templateUrl: './left-navigation.component.html',
    styleUrls: ['./left-navigation.component.scss']
})
export class LeftNavigationComponent implements OnInit {

    public emailList: Email[] = [];
    public inboxList;

    constructor(public fetchData: FetchDataService, public api: ApiService) { }

    selectMailbox(accountId: Number){
        Storage.set('selected_email', accountId.toString());
        this.api.nextMailAccount();
    }

    ngOnInit() {

        this.fetchData.selectedMailBox(parseInt(Storage.get('emailList'))).then(emailList => this.inboxList = emailList);

        this.fetchData.emailAccountList.subscribe((res: Email[]) => {
            this.emailList = res;
        })
        // this.emailList = JSON.parse(Storage.get('emailList'));
        $('#leftNavToggleBtn').on('click', () => {
            $('#left-navigation').toggleClass('leftNavExpand');
        });
    }

}
