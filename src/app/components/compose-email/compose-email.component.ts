import { Component, OnInit } from '@angular/core';
import { ComposeEmail } from '../../services/api/contract';
import { ApiService } from '../../services/api.service';
import { FirebaseAuthService } from '../../services/firebase.auth.service';
import { Response } from '../../services/api/contract';
declare var $: any;

@Component({
    selector: 'heyooo-compose-email',
    templateUrl: './compose-email.component.html',
    styleUrls: ['./compose-email.component.scss']
})
export class ComposeEmailComponent implements OnInit {

    contactList: string[] = [
        'test@jnexsoft.com',
        'testing@jnexsoft.com'
    ];

    public emailList: any[] = [];

    public email_id: number;
    public subject: string;
    public from: number;

    constructor(private api: ApiService, private auth: FirebaseAuthService) { }



    composeEmail(from, subject) {

        const body = $('textarea').val();
        const to = ($('#to').select2('data'));
        const cc = ($('#cc').select2('data'));
        const bcc = ($('#bcc').select2('data'));

        const data: ComposeEmail = {
            'to': to.map(element => element.text),
            'email_id': from,
            'body': body,
            'subject': subject,
            'cc': cc.map(element => element.text),
            'bcc': bcc.map(element => element.text),
        };
        this.api.sendEmail(data).subscribe((res: Response) => {
            alert(res.data.msg);
        });
    }

    ngOnInit() {
        $('.js-example-basic-multiple').select2({
            width: '100%',
            tags: true,
        });

        $('.select2-selection--multiple')
            .css('height', '35px')
            .css('border-color', '#dfe3ec');

        this.api.fetchEmails().subscribe((res: Response) => {
            this.emailList = res.data.emails;
        });
    }
}
