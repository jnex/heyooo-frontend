import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'heyooo-wysiwyg-editor',
  templateUrl: './wysiwyg-editor.component.html',
  styleUrls: ['./wysiwyg-editor.component.scss']
})
export class WysiwygEditorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
      $.trumbowyg.svgPath = '/assets/trumbowyg-icons.svg';
      $('#trumbowyg-demo').trumbowyg({});
  }

}
