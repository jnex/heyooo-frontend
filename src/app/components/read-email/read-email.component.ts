import { Component, OnInit } from '@angular/core';
import { FetchDataService } from '../../services/fetch-data.service';
import Storage from '../../services/storage.service';
import { Email, EmailResponse } from '../../../interface';
import { KeepHtmlPipe } from '../../pipes/keep-html.pipe';
import { StringToDatePipe } from '../../pipes/string-to-date.pipe';

@Component({
    selector: 'heyooo-read-email',
    templateUrl: './read-email.component.html',
    styleUrls: ['./read-email.component.scss']
})
export class ReadEmailComponent implements OnInit {

    currentEmail;

    constructor(private fetchData: FetchDataService) { }

    ngOnInit() {
        this.fetchData.readCurrentEmail.subscribe(res => {
            this.currentEmail = null;
            const currEmailId = parseInt(Storage.get('selected_email'));
            this.fetchData.selectedMailBox(currEmailId).then(emailList => {
                this.currentEmail = emailList[res];
                // console.log(this.currentEmail)
            });
        })
    }

}
