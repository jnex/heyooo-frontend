import { Component, OnInit } from '@angular/core';
import { FirebaseAuthService } from '../../services/firebase.auth.service';
import { Router } from '@angular/router';
declare var $: any;

@Component({
    selector: 'heyooo-top-navigation',
    templateUrl: './top-navigation.component.html',
    styleUrls: ['./top-navigation.component.scss']
})
export class TopNavigationComponent implements OnInit {

    constructor(public auth: FirebaseAuthService, private router: Router) { }

    ngOnInit() {
        $('.dropdown-toggle').dropdown()
    }

    logout(): void {
        this.auth.logout();
        this.router.navigate(['/signin']);
    }

}
